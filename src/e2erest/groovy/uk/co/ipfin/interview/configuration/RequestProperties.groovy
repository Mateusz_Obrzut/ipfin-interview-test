package uk.co.ipfin.interview.configuration

class RequestProperties {

    Map<String, String> properties

    RequestProperties (){
        Properties fileProperties = new Properties()
        this.getClass().getResource('/jsonplaceholder-typicode.properties').withInputStream {
            it -> fileProperties.load(it)
        }

        properties = new HashMap<>(fileProperties)
    }
}
