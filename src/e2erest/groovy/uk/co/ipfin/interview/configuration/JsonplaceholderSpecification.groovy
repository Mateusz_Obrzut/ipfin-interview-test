package uk.co.ipfin.interview.configuration

import spock.lang.Shared
import spock.lang.Specification

class JsonplaceholderSpecification extends Specification {

    @Shared requestProperties = new RequestProperties()
}
