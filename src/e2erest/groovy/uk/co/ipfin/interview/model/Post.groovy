package uk.co.ipfin.interview.model

import com.fasterxml.jackson.annotation.JsonProperty
import groovy.transform.builder.Builder

@Builder(allProperties = false)
class Post {
    int userId
    String title
    String body

    /**
     * access was restricted to WRITE_ONLY to exclude id field during serialization
     * otherwise it would be sent in request body with default value == 0
     */
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    int id

}
