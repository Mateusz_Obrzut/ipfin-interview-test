package uk.co.ipfin.interview

import uk.co.ipfin.interview.configuration.JsonplaceholderSpecification
import uk.co.ipfin.interview.model.Post

import static io.restassured.RestAssured.given
import static io.restassured.http.Method.GET

class RetrieveMaxUserIdScenario extends JsonplaceholderSpecification {

    def "Send GET request to /posts resource and retrieve max value of userId"() {
        setup: "Expected userId max value (10) is set"
            int expectedUserIdMaxValue = 10

        when: "GET request is sent"

        then: "Response status is 200"
            List<Post> posts = given()
                                    .baseUri(requestProperties.getProperties().get('url'))
                                    .basePath(requestProperties.getProperties().get('postsResourcePath'))
                                .when()
                                    .log().all()
                                    .request(GET)
                                .then()
                                    .log().all()
                                    .statusCode(200)
                                    .extract().body().as(Post[].class)

        and: "Actual userId max value is equal to expected one (10)"
            int actualUserIdMaxValue = posts.collect {it.userId}.max()
            assert expectedUserIdMaxValue == actualUserIdMaxValue
    }
}
