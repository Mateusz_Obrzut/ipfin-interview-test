package uk.co.ipfin.interview

import uk.co.ipfin.interview.configuration.JsonplaceholderSpecification

import static io.restassured.http.Method.GET
import uk.co.ipfin.interview.model.Post

import static io.restassured.RestAssured.given

class RetrievePostInfoScenario extends JsonplaceholderSpecification {

    private static final USER_ID_QUERY_PARAM = "userId"

    def "Send GET request to /posts resource passing userId and retrieve max value of post's id"() {
        setup: "Expected max id (90) and userId values are set"
            int userId = Integer.parseInt(requestProperties.getProperties().get('userId'))
            int expectedMaxId = 90

        when: "GET request is sent"

        then: "Response status is 200"
            List<Post> postsOfSpecificUser = given()
                                                .baseUri(requestProperties.getProperties().get('url'))
                                                .basePath(requestProperties.getProperties().get('postsResourcePath'))
                                                .queryParam(USER_ID_QUERY_PARAM, userId)
                                            .when()
                                                .log().all()
                                                .request(GET)
                                            .then()
                                                .log().all()
                                                .statusCode(200)
                                                .extract().body().as(Post[].class)

        and: "Actual max id value is equal to expected one (90)"
            int actualMaxId = postsOfSpecificUser.collect {it.id}.max()
            assert expectedMaxId == actualMaxId
    }
}
