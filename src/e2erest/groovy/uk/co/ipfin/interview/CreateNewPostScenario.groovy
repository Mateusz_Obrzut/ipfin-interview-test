package uk.co.ipfin.interview

import uk.co.ipfin.interview.configuration.JsonplaceholderSpecification
import uk.co.ipfin.interview.model.Post

import static io.restassured.RestAssured.given
import static io.restassured.http.Method.POST

class CreateNewPostScenario extends JsonplaceholderSpecification {

    def "Send POST request to /posts resource to create new post"() {
        setup: "New Post object is created"
            Post newPost = Post.builder()
                            .userId(Integer.parseInt(requestProperties.getProperties().get('userId')))
                            .title("foo")
                            .body("bar")
                            .build()

        when: "POST request is sent"

        then: "Response status is 201"
            Post response = given()
                                .baseUri(requestProperties.getProperties().get('url'))
                                .basePath(requestProperties.getProperties().get('postsResourcePath'))
                                .body(newPost)
                            .when()
                                .log().all()
                                .request(POST)
                            .then()
                                .log().all()
                                .statusCode(201)
                                .extract().body().as(Post.class)

        and: "Returned identifier is greater than 100"
            assert response.id > 100
    }
}
