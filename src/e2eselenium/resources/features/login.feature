Feature: Login functionality
  As an User I want to be able to login into application successfully passing valid credentials
  so that I could access "My account page".
  If I pass invalid credentials the login process fails warning me about incorrect username or password

  Background:
    Given Home page is displayed
    When User clicks Sign in button
    Then Authentication page is displayed

  @Selenium
  Scenario: User provides valid credentials and logs into application successfully
    When User provides email address and password
      | test123@test789.com | test123 |
    And User confirms credentials clicking Sign in button
    Then User's information is displayed at My Account page
      | Test Test |

  @Selenium
  Scenario: User provides invalid credentials and fails to log into application
    When User provides email address and password
      | test123@test789.com | password |
    And User confirms credentials clicking Sign in button
    Then Error message about invalid credentials is displayed
