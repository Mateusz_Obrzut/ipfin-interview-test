Feature: Registration functionality
  As an User I want to be able to register a new account passing required information
  so that I could have full access to the application.
  If I pass data in invalid format the registration process fails with information what went wrong.

  Background:
    Given Home page is displayed
    When User clicks Sign in button
    Then Authentication page is displayed

  @Selenium
  Scenario Outline: User provides all required information and successfully registers new account
    When User provides email address for new account as "<email>"
    And User clicks Create an account button
    Then Create an account page is displayed
    When User provides first name as "<firstName>"
    And User provides last name as "<lastName>"
    And User provides password as "<password>"
    And User provides address as "<address>"
    And User provides city as "<city>"
    And User provides state as "<state>"
    And User provides postal code as "<postalCode>"
    And User provides mobile phone number as "<mobilePhoneNumber>"
    And User clicks Register button
    Then My Account page is displayed

    Examples:
      |       email     | firstName | lastName | password |     address        |   city  |  state  | postalCode | mobilePhoneNumber |
      | test@sample.com |   John    |   White  | password | Test address 123/3 | Atlanta | Georgia |    12345   |     666999666     |
