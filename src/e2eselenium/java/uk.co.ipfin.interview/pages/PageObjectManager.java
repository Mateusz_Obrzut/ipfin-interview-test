package uk.co.ipfin.interview.pages;

import org.openqa.selenium.WebDriver;

public class PageObjectManager {
    private WebDriver driver;
    private HomePage homePage;
    private AuthenticationPage authenticationPage;
    private NewAccountPage newAccountPage;
    private MyAccountPage myAccountPage;

    public PageObjectManager(WebDriver driver){
        this.driver = driver;
    }

    public HomePage getHomePage(){
        return homePage == null ? new HomePage(driver) : this.homePage;
    }

    public AuthenticationPage getAuthenticationPage(){
        return authenticationPage == null ? new AuthenticationPage(driver) : this.authenticationPage;
    }

    public NewAccountPage getNewAccountPage(){
        return newAccountPage == null ? new NewAccountPage(driver) : this.newAccountPage;
    }

    public MyAccountPage getMyAccountPage(){
        return myAccountPage == null ? new MyAccountPage(driver) : this.myAccountPage;
    }
}
