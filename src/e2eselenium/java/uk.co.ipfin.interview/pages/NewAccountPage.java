package uk.co.ipfin.interview.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class NewAccountPage extends BasePage {
    private static final String PAGE_HEADING_TEXT = "create an account";

    @FindBy(tagName = "h1")
    private WebElement heading;

    @FindBy(id = "uniform-id_gender2")
    private WebElement womanRadioButton;

    @FindBy(id = "customer_firstname")
    private WebElement firstNameField;

    @FindBy(id = "customer_lastname")
    private WebElement lastNameField;

    @FindBy(id = "passwd")
    private WebElement passwordField;

    @FindBy(id = "address1")
    private WebElement addressField;

    @FindBy(id = "city")
    private WebElement cityField;

    @FindBy(id = "id_state")
    private WebElement stateDropDownList;

    @FindBy(id = "postcode")
    private WebElement postalCodeField;

    @FindBy(id = "phone_mobile")
    private WebElement mobilePhoneField;

    @FindBy(id = "submitAccount")
    private WebElement registerButton;

    public NewAccountPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void checkWomanRadioButton(){
        womanRadioButton.click();
    }

    public void provideFirstName(String firstName){
        clearAndSetField(firstNameField, firstName);
    }

    public void provideLastName(String lastName){
        clearAndSetField(lastNameField, lastName);
    }

    public void providePassword(String password){
        clearAndSetField(passwordField, password);
    }

    public void provideAddress(String address){
        clearAndSetField(addressField, address);
    }

    public void provideCity(String city){
        clearAndSetField(cityField, city);
    }

    public void selectStateByVisibleText(String state){
        Select states = new Select(stateDropDownList);
        states.selectByVisibleText(state);
    }

    public void providePostalCodeField(String postalCode){
        postalCodeField.clear();
        postalCodeField.sendKeys(postalCode);
    }

    public void provideMobilePhoneField(String mobileNumber){
        mobilePhoneField.clear();
        mobilePhoneField.sendKeys(mobileNumber);
    }

    public void clickRegisterButton(){
        registerButton.click();
    }

    @Override
    public boolean isDisplayed(){
        waitForElement(driver, firstNameField);
        return heading.getText().toLowerCase().contains(PAGE_HEADING_TEXT);
    }
}
