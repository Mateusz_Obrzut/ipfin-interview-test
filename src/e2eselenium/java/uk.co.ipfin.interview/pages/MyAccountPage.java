package uk.co.ipfin.interview.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyAccountPage extends BasePage {
    private static final String PAGE_HEADING_TEXT = "my account";

    @FindBy(tagName = "h1")
    private WebElement heading;

    @FindBy(xpath = "//*[@id=\"header\"]/div[2]/div/div/nav/div[1]/a/span")
    private WebElement userInformation;

    public MyAccountPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public boolean isUserLoggedIn(String firstNameAndLastName){
        return userInformation.getText().equals(firstNameAndLastName);
    }

    @Override
    public boolean isDisplayed(){
        return heading.getText().toLowerCase().contains(PAGE_HEADING_TEXT);
    }
}
