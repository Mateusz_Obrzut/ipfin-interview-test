package uk.co.ipfin.interview.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AuthenticationPage extends BasePage {
    private static final String PAGE_HEADING_TEXT = "authentication";
    private static final String AUTHENTICATION_ERROR_MESSAGE = "authentication failed.";

    @FindBy(id = "email_create")
    private WebElement newUserEmailField;

    @FindBy(id = "SubmitCreate")
    private WebElement createAccountButton;

    @FindBy(id = "email")
    private WebElement existingUserEmailField;

    @FindBy(id = "passwd")
    private WebElement existingUserPasswordField;

    @FindBy(id = "SubmitLogin")
    private WebElement signInButton;

    @FindBy(tagName = "h1")
    private WebElement heading;

    @FindBy(xpath = "//*[@id=\"center_column\"]/div[1]/ol/li")
    private WebElement authenticationError;

    public AuthenticationPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void provideNewUserEmail(String emailAddress) {
        clearAndSetField(newUserEmailField, emailAddress);
    }

    public void clickCreateAccountButton(){
        createAccountButton.click();
    }

    public void provideExistingUserEmail(String emailAddress){
        clearAndSetField(existingUserEmailField, emailAddress);
    }

    public void provideExistingUserPassword(String password){
        clearAndSetField(existingUserPasswordField, password);
    }

    public void clickSignInButton(){
        signInButton.click();
    }

    public boolean isAuthenticationErrorDisplayed(){
        return authenticationError.getText().toLowerCase().equals(AUTHENTICATION_ERROR_MESSAGE);
    }

    @Override
    public boolean isDisplayed(){
        return heading.getText().toLowerCase().contains(PAGE_HEADING_TEXT);
    }

}
