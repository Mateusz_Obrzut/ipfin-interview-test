package uk.co.ipfin.interview.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.NoSuchElementException;

abstract class BasePage {
    WebDriver driver;

    void clearAndSetField(WebElement webElement, String value){
        webElement.clear();
        webElement.sendKeys(value);
    }

    abstract boolean isDisplayed();

    void waitForElement(WebDriver driver, WebElement webElement){
       FluentWait<WebDriver> fluentWait = new FluentWait<>(driver)
                                            .withTimeout(Duration.ofSeconds(10))
                                            .pollingEvery(Duration.ofSeconds(1))
                                            .ignoring(NoSuchElementException.class);

       fluentWait.until(ExpectedConditions.visibilityOf(webElement));
    }
}
