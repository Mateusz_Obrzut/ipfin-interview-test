package uk.co.ipfin.interview.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {
    public static final String PAGE_URL = "http://automationpractice.com/index.php";

    @FindBy(partialLinkText="Sign in")
    private WebElement signInButton;

    @FindBy(id = "search_query_top")
    private WebElement searchField;

    public HomePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void openAuthenticationPage(){
        signInButton.click();
    }

    @Override
    public boolean isDisplayed(){
         return searchField.isDisplayed();
    }
}
