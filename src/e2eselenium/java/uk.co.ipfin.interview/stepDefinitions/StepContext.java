package uk.co.ipfin.interview.stepDefinitions;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.WebDriver;
import uk.co.ipfin.interview.configuration.DriverType;
import uk.co.ipfin.interview.configuration.WebDriverFactory;
import uk.co.ipfin.interview.pages.PageObjectManager;

public class StepContext {
    private static boolean initialized = false;
    private WebDriver driver;
    private PageObjectManager pageObjectManager;

    @Before("@Selenium")
    public void setup(){
        if(!initialized){
            driver = WebDriverFactory.getWebDriver(DriverType.CHROME);
            pageObjectManager = new PageObjectManager(driver);
            initialized = true;
        }
    }

    @After("@Selenium")
    public void tearDown(){
        driver.quit();
        initialized = false;
        pageObjectManager = null;
    }

    public WebDriver getDriver(){
        return this.driver;
    }

    PageObjectManager getPageObjectManager(){
        return this.pageObjectManager;
    }
}
