package uk.co.ipfin.interview.stepDefinitions;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import uk.co.ipfin.interview.pages.AuthenticationPage;
import uk.co.ipfin.interview.pages.MyAccountPage;
import uk.co.ipfin.interview.pages.NewAccountPage;
import uk.co.ipfin.interview.pages.PageObjectManager;

import java.util.Random;

public class RegistrationSteps {
    private PageObjectManager pageObjectManager;
    private AuthenticationPage authenticationPage;
    private NewAccountPage newAccountPage;
    private MyAccountPage myAccountPage;

    public RegistrationSteps(StepContext stepContext){
        pageObjectManager = stepContext.getPageObjectManager();
    }

    @When("^User provides email address for new account as \"([^\"]*)\"$")
    public void user_provides_email_address_for_new_account_as(String emailAddress){
        String newEmailAddress = emailAddress.replace("test", "test" + this.generateRandomNumber());
        authenticationPage = pageObjectManager.getAuthenticationPage();
        authenticationPage.provideNewUserEmail(newEmailAddress);
    }

    @When("^User clicks Create an account button$")
    public void user_clicks_Create_an_account_button(){
        authenticationPage.clickCreateAccountButton();
        newAccountPage = pageObjectManager.getNewAccountPage();
    }

    @Then("^Create an account page is displayed$")
    public void create_an_account_page_is_displayed(){
        assert newAccountPage.isDisplayed();
    }

    @When("^User provides first name as \"([^\"]*)\"$")
    public void user_provides_first_name_as(String firstName){
        newAccountPage.provideFirstName(firstName);
    }

    @When("^User provides last name as \"([^\"]*)\"$")
    public void user_provides_last_name_as(String lastName){
        newAccountPage.provideLastName(lastName);
    }

    @When("^User provides password as \"([^\"]*)\"$")
    public void user_provides_password_as(String password){
        newAccountPage.providePassword(password);
    }

    @When("^User provides address as \"([^\"]*)\"$")
    public void user_provides_address_as(String address){
        newAccountPage.provideAddress(address);
    }

    @When("^User provides city as \"([^\"]*)\"$")
    public void user_provides_city_as(String city){
        newAccountPage.provideCity(city);
    }

    @When("^User provides state as \"([^\"]*)\"$")
    public void user_provides_state_as(String state){
        newAccountPage.selectStateByVisibleText(state);
    }

    @When("^User provides postal code as \"([^\"]*)\"$")
    public void user_provides_postal_code_as(String postalCode){
        newAccountPage.providePostalCodeField(postalCode);
    }

    @When("^User provides mobile phone number as \"([^\"]*)\"$")
    public void user_provides_mobile_phone_number_as(String mobilePhoneNumber){
        newAccountPage.provideMobilePhoneField(mobilePhoneNumber);
    }

    @When("^User clicks Register button$")
    public void user_clicks_Register_button(){
        newAccountPage.clickRegisterButton();
        myAccountPage = pageObjectManager.getMyAccountPage();
    }

    @Then("^My Account page is displayed$")
    public void my_Account_page_is_displayed(){
        myAccountPage.isDisplayed();
    }

    private String generateRandomNumber() {
        return Integer.toString(new Random().nextInt(99999999) + 1000);
    }
}
