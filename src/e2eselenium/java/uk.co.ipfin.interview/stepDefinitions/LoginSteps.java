package uk.co.ipfin.interview.stepDefinitions;

import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import uk.co.ipfin.interview.pages.AuthenticationPage;
import uk.co.ipfin.interview.pages.MyAccountPage;
import uk.co.ipfin.interview.pages.PageObjectManager;

import java.util.List;

public class LoginSteps {
    private PageObjectManager pageObjectManager;
    private AuthenticationPage authenticationPage;
    private MyAccountPage myAccountPage;

    public LoginSteps(StepContext stepContext){
        this.pageObjectManager = stepContext.getPageObjectManager();
    }


    @When("^User provides email address and password$")
    public void user_provides_email_address_and_password(DataTable dataTable){
        List<List<String>> testData = dataTable.raw();
        authenticationPage = pageObjectManager.getAuthenticationPage();
        authenticationPage.provideExistingUserEmail(testData.get(0).get(0));
        authenticationPage.provideExistingUserPassword(testData.get(0).get(1));
    }

    @When("^User confirms credentials clicking Sign in button$")
    public void user_confirms_credentials_clicking_Sign_in_button(){
        authenticationPage.clickSignInButton();
        myAccountPage = pageObjectManager.getMyAccountPage();
    }

    @Then("^User's information is displayed at My Account page$")
    public void user_s_information_is_displayed_at_My_Account_page(DataTable dataTable){
        List<List<String>> testData = dataTable.raw();
        assert myAccountPage.isUserLoggedIn(testData.get(0).get(0));
    }

    @Then("^Error message about invalid credentials is displayed$")
    public void error_message_about_invalid_credentials_is_displayed(){
        assert authenticationPage.isAuthenticationErrorDisplayed();
    }
}
