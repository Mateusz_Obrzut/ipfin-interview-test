package uk.co.ipfin.interview.stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import uk.co.ipfin.interview.pages.AuthenticationPage;
import uk.co.ipfin.interview.pages.HomePage;
import uk.co.ipfin.interview.pages.PageObjectManager;

import static uk.co.ipfin.interview.pages.HomePage.PAGE_URL;

public class CommonSteps {
    private WebDriver driver;
    private PageObjectManager pageObjectManager;
    private HomePage homePage;
    private AuthenticationPage authenticationPage;

    public CommonSteps(StepContext stepContext){
        this.driver = stepContext.getDriver();
        this.pageObjectManager = stepContext.getPageObjectManager();
    }

    @Given("^Home page is displayed$")
    public void home_page_is_displayed(){
        driver.get(PAGE_URL);
        homePage = pageObjectManager.getHomePage();
        homePage.isDisplayed();
    }

    @When("^User clicks Sign in button$")
    public void user_clicks_Sign_in_button(){
        homePage.openAuthenticationPage();
        authenticationPage = pageObjectManager.getAuthenticationPage();
    }

    @Then("^Authentication page is displayed$")
    public void authentication_page_is_displayed(){
        assert authenticationPage.isDisplayed();
    }

}
