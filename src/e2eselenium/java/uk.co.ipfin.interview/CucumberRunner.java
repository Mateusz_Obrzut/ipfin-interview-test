package uk.co.ipfin.interview;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(format = {"pretty","html:reports/test-report"},
                    features = "classpath:features",
                    glue={"classpath:uk.co.ipfin.interview/stepDefinitions"},
                    tags = {"@Selenium"})
public class CucumberRunner {
}
