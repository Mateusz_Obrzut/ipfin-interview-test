package uk.co.ipfin.interview.configuration;

import org.openqa.selenium.WebDriver;

abstract class ExecutableWebDriver {
    WebDriver driver;

    abstract void setDriver();

    public WebDriver getDriver(){
        setDriver();
        return this.driver;
    }

    /**
     * it seems that there is a problem with file permissions for UNIX machines:
     * https://stackoverflow.com/questions/6521132/keep-permissions-on-files-with-maven-resourcestestresources
     * as a quick workaround the file is taken directly from resources
     */
    String getPathToWebDriverExecutableFile(String driverName){
        String pathToChromeDriver;
        switch(OperatingSystem.getOperatingSystem(System.getProperty("os.name"))){
            case WINDOWS:
                pathToChromeDriver = getClass().getResource("/webdrivers/windows/") + driverName + ".exe";
                break;
            default:
                pathToChromeDriver = "./src/e2eselenium/resources/webdrivers/mac/" + driverName;
                break;
        }
        return pathToChromeDriver;
    }

}
