package uk.co.ipfin.interview.configuration;

import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class FirefoxWebDriver extends ExecutableWebDriver {
    private static final String DRIVER_NAME = "geckodriver";

    @Override
    public void setDriver(){
        System.setProperty("webdriver.gecko.driver", getPathToWebDriverExecutableFile(DRIVER_NAME));
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }
}
