package uk.co.ipfin.interview.configuration;

import java.util.stream.Stream;

public enum OperatingSystem {
    MAC("mac"),
    WINDOWS("windows");
    /**
     * add more operating systems if needed
     */

    private String systemName;

    private OperatingSystem(String systemName){
        this.systemName = systemName;
    }

    public String getSystemName(){
        return this.systemName;
    }

    public static OperatingSystem getOperatingSystem(String fullSystemName){
        return Stream.of(OperatingSystem.values())
                .filter(x -> fullSystemName.toLowerCase().contains(x.getSystemName()))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
