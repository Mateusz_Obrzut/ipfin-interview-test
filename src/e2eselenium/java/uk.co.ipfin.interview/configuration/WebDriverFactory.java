package uk.co.ipfin.interview.configuration;

import org.openqa.selenium.WebDriver;

public class WebDriverFactory {

    public static WebDriver getWebDriver(DriverType driverType){
        WebDriver driver;

        switch(driverType){
            case FIREFOX:
                driver = new FirefoxWebDriver().getDriver();
            default:
                driver = new ChromeWebDriver().getDriver();
        }

        return driver;
    }
}
