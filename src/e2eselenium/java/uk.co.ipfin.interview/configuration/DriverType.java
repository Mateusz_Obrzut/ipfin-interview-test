package uk.co.ipfin.interview.configuration;

public enum DriverType {
    CHROME,
    FIREFOX;

    /**
     * add more driver types if needed
     */
}
