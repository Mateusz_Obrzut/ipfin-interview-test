Feature: Withdrawal functionality
  As an bank customer I want be able to make money withdrawals from my private account
  so that I can spend my money whenever I want.
  If specified amount to withdraw does not exceed the amount of money currently present at my bank account
  then withdrawal operation should be successful.
  If specified amount to withdraw exceeds the amount of money currently present at my bank account
  the withdrawal operation fails and system warns me with specific error message.

  Background: User has deposited 200 PLN to its private bank account
    Given Bank account with 200.00 PLN deposited

  @Unit-test
  Scenario Outline: Unsuccessful withdrawal
    When User wants to withdrawn some money <amountToWithdraw>
    Then System throws error message
    And Withdrawal amount is equal to <withdrawnAmount>
    And Outstanding amount at his account is <outstandingAmount>

    Examples:
      | amountToWithdraw  | withdrawnAmount | outstandingAmount |
      |         500.00    |       0.00      |       200.00      |

  @Unit-test
  Scenario Outline: Successful withdrawal
    When User wants to withdrawn some money <amountToWithdraw>
    Then Withdrawal amount is equal to <withdrawnAmount>
    Then Outstanding amount at his account is <outstandingAmount>

    Examples:
      | amountToWithdraw  | withdrawnAmount  | outstandingAmount  |
      |         0.00      |       0.00       |       200.00       |
      |         50.00     |       50.00      |       150.00       |
      |         100.00    |       100.00     |       100.00       |
      |         150.00    |       150.00     |       50.00        |
      |         200.00    |       200.00     |       0.00         |
