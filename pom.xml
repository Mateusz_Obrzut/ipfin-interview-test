<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>uk.co.ipfin.interview</groupId>
    <artifactId>ipfin-interview-test</artifactId>
    <version>1.0-SNAPSHOT</version>

    <name>ipfin-interview-test</name>
    <url>https://www.ipfin.co.uk/en/index.html</url>

    <properties>
        <!-- Project properties -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <java.version>1.8</java.version>
        <skip.unit.tests>false</skip.unit.tests>
        <!-- Test dependencies -->
        <spock-core.version>1.2-groovy-2.5</spock-core.version>
        <spock-reports.version>1.6.1</spock-reports.version>
        <groovy-all.version>2.5.2</groovy-all.version>
        <rest-assured.version>3.1.1</rest-assured.version>
        <hamcrest-all.version>1.3</hamcrest-all.version>
        <jackson.version>2.9.7</jackson.version>
        <junit.version>4.12</junit.version>
        <selenium.version>3.14.0</selenium.version>
        <cucumber.version>1.2.5</cucumber.version>
        <cucumber-picocontainer.version>1.2.5</cucumber-picocontainer.version>
        <!-- Plugins -->
        <maven-clean-plugin.version>3.0.0</maven-clean-plugin.version>
        <maven-compiler-plugin.version>3.7.0</maven-compiler-plugin.version>
        <maven-surefire-plugin.version>2.22.0</maven-surefire-plugin.version>
        <gmavenplus-plugin.version>1.6.1</gmavenplus-plugin.version>
        <build-helper-maven-plugin.version>3.0.0</build-helper-maven-plugin.version>
        <maven-resource-plugin.version>3.0.2</maven-resource-plugin.version>
        <maven-install-plugin.version>2.5.2</maven-install-plugin.version>
    </properties>

    <profiles>
        <profile>
            <id>E2EREST</id>
            <properties>
                <skip.unit.tests>true</skip.unit.tests>
            </properties>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>

            <build>
                <plugins>

                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-failsafe-plugin</artifactId>
                        <version>${maven-surefire-plugin.version}</version>
                        <configuration>
                            <testSourceDirectory>src/e2erest/groovy</testSourceDirectory>
                            <includes>
                                <include>**/*Scenario.java</include>
                            </includes>
                        </configuration>
                        <executions>
                            <execution>
                                <id>integration-tests</id>
                                <goals>
                                    <goal>integration-test</goal>
                                    <goal>verify</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>

                    <plugin>
                        <groupId>org.codehaus.gmavenplus</groupId>
                        <artifactId>gmavenplus-plugin</artifactId>
                        <version>${gmavenplus-plugin.version}</version>
                        <configuration>
                            <testSources>
                                <testSource>
                                    <directory>src/e2erest/groovy</directory>
                                    <includes>
                                        <include>**/*.groovy</include>
                                    </includes>
                                </testSource>
                            </testSources>
                        </configuration>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>addTestSources</goal>
                                    <goal>compileTests</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>

                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>build-helper-maven-plugin</artifactId>
                        <version>${build-helper-maven-plugin.version}</version>
                        <executions>
                            <execution>
                                <id>add-test-resources</id>
                                <phase>generate-test-resources</phase>
                                <goals>
                                    <goal>add-test-resource</goal>
                                </goals>
                                <configuration>
                                    <resources>
                                        <resource>
                                            <directory>src/e2erest/resources</directory>
                                        </resource>
                                    </resources>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>

                </plugins>
            </build>

        </profile>

        <profile>
            <id>E2ESELENIUM</id>
            <properties>
                <skip.unit.tests>true</skip.unit.tests>
            </properties>
            <activation>
                <activeByDefault>false</activeByDefault>
            </activation>

            <build>
                <plugins>

                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>build-helper-maven-plugin</artifactId>
                        <version>${build-helper-maven-plugin.version}</version>
                        <executions>
                            <execution>
                                <id>add-selenium-test-sources</id>
                                <phase>generate-test-sources</phase>
                                <goals>
                                    <goal>add-test-source</goal>
                                </goals>
                                <configuration>
                                    <sources>
                                        <source>src/e2eselenium/java</source>
                                    </sources>
                                </configuration>
                            </execution>
                            <execution>
                                <id>add-selenium-test-resources</id>
                                <phase>generate-test-resources</phase>
                                <goals>
                                    <goal>add-test-resource</goal>
                                </goals>
                                <configuration>
                                    <resources>
                                        <resource>
                                            <filtering>true</filtering>
                                            <directory>src/e2eselenium/resources</directory>
                                        </resource>
                                    </resources>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>

                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-failsafe-plugin</artifactId>
                        <version>${maven-surefire-plugin.version}</version>
                        <configuration>
                            <testSourceDirectory>src/e2selenium/java</testSourceDirectory>
                            <includes>
                                <include>**/*CucumberRunner.java</include>
                            </includes>
                        </configuration>
                        <executions>
                            <execution>
                                <id>integration-tests</id>
                                <goals>
                                    <goal>integration-test</goal>
                                    <goal>verify</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>

                </plugins>
            </build>

        </profile>
    </profiles>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <artifactId>maven-clean-plugin</artifactId>
                    <version>${maven-clean-plugin.version}</version>
                </plugin>
                <plugin>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>${maven-resource-plugin.version}</version>
                </plugin>
                <plugin>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>${maven-compiler-plugin.version}</version>
                    <configuration>
                        <source>${java.version}</source>
                        <target>${java.version}</target>
                    </configuration>
                </plugin>
                <plugin>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <version>${maven-surefire-plugin.version}</version>
                    <configuration>
                        <skipTests>${skip.unit.tests}</skipTests>
                    </configuration>
                </plugin>
                <plugin>
                    <artifactId>maven-install-plugin</artifactId>
                    <version>${maven-install-plugin.version}</version>
                </plugin>

            </plugins>
        </pluginManagement>
    </build>

    <dependencies>
        <!-- Spock -->
        <dependency>
            <groupId>org.spockframework</groupId>
            <artifactId>spock-core</artifactId>
            <version>${spock-core.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.athaydes</groupId>
            <artifactId>spock-reports</artifactId>
            <version>${spock-reports.version}</version>
            <scope>test</scope>
        </dependency>
        <!-- Groovy -->
        <dependency>
            <groupId>org.codehaus.groovy</groupId>
            <artifactId>groovy-all</artifactId>
            <version>${groovy-all.version}</version>
            <type>pom</type>
            <scope>test</scope>
        </dependency>
        <!-- RestAssured -->
        <dependency>
            <groupId>io.rest-assured</groupId>
            <artifactId>rest-assured</artifactId>
            <version>${rest-assured.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-all</artifactId>
            <version>${hamcrest-all.version}</version>
            <scope>test</scope>
        </dependency>
        <!-- Jackson -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-annotations</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <!-- JUnit -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
        <!-- Cucumber -->
        <dependency>
            <groupId>info.cukes</groupId>
            <artifactId>cucumber-java</artifactId>
            <version>${cucumber.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>info.cukes</groupId>
            <artifactId>cucumber-junit</artifactId>
            <version>${cucumber.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>info.cukes</groupId>
            <artifactId>cucumber-jvm</artifactId>
            <version>${cucumber.version}</version>
             <type>pom</type>
        </dependency>
        <dependency>
            <groupId>info.cukes</groupId>
            <artifactId>cucumber-picocontainer</artifactId>
            <version>${cucumber-picocontainer.version}</version>
            <scope>test</scope>
        </dependency>
        <!-- Selenium -->
        <dependency>
            <groupId>org.seleniumhq.selenium</groupId>
            <artifactId>selenium-java</artifactId>
            <version>${selenium.version}</version>
        </dependency>
        <dependency>
            <groupId>org.seleniumhq.selenium</groupId>
            <artifactId>selenium-support</artifactId>
            <version>${selenium.version}</version>
        </dependency>
        <dependency>
            <groupId>org.seleniumhq.selenium</groupId>
            <artifactId>selenium-firefox-driver</artifactId>
            <version>${selenium.version}</version>
        </dependency>
    </dependencies>

</project>
