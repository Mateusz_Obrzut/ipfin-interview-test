# ipfin-interview-test
Test scenarios prepared for interview purposes.

## How to execute tests and verify reports
This project contains several test scenarios written in different languages and with use of different frameworks:
* Java and Selenium
* Groovy, REST Assured and Spock
* Cucumber feature file

### Test scenarios for REST service
Tests for REST service were prepared using: Groovy, Spock framework and REST Assured library.
To run test scenarios please execute maven's `verify` phase passing `E2EREST` profile: `mvn clean verify -P E2EREST`
Reports are located in: `/build/spock-reports` folder inside project's root directory

### Test scenarios for Selenium
Tests for Selenium were prepared using: Java, Selenium and Cucumber.
To run test scenarios please execute maven's `verify` phase passing `E2ESELENIUM` profile: `mvn clean verify -P E2ESELENIUM`
Reports are located in: `/reports/test-report` folder inside project's root directory

### Test scenarios for Cucumber
Feature file for withdrawal scenarios is located under `src/test/resources/features`